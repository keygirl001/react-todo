import React from 'react';
import { Component } from 'react';

class TodoLlist extends React.Component {
  constructor() {
    super();
  }
  render() {
    const items = this.props.items;
    const itemList = items.map(item => (
      <li key={item.id}>{item.text}</li>
    ))
    console.log(items);
    return (
      <ul>
        {itemList}
      </ul>
    )
  }
}

export default TodoLlist;
