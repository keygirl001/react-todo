import React from 'react';
import { Component } from 'react';
import TodoList from './TodoList';
import Remarkable from 'remarkable';
import RemarkableReactRenderer from 'react-remarkable';

class TodoApp extends Component {
  constructor() {
    super();
    this.state = {text: '', items: [], value: 'Type some *markdown* here!'};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeMark = this.handleChangeMark.bind(this);
    this.getRawMarkup = this.getRawMarkup.bind(this);
  }

  render() {
   return (
    <div>
      <h3>TODO</h3>
      <form onSubmit={this.handleSubmit}>
        <input 
          onChange={this.handleChange}
          value={this.state.text}
        ></input>
        <button>Add #{this.state.items.length + 1}</button>
      </form>
      <TodoList items={this.state.items}/>
      <div className="markdown">
        <h3>Input</h3>
        <textarea
          onChange={this.handleChangeMark}
          defaultValue={this.state.value}
        />
        <h3>Output</h3>
        <div
          className="content"
          dangerouslySetInnerHTML={this.getRawMarkup()}
        />
      </div>
    </div>
   )
  }
  
  handleChangeMark(e) {
    this.setState({
      value: e.target.value
    })
  }

  getRawMarkup() {
    const md = new Remarkable();
    return { __html: md.render(this.state.value) };
  }

  handleChange(e) {
    this.setState({
      text: e.target.value,
    })
  }

  handleSubmit(e) {
    e.preventDefault();
    const newItem = {
      text: this.state.text,
      id: Date.now()
    };
    this.setState(prevState => ({
      items: prevState.items.concat(newItem),
      text: ''
    }))
  }
}

export default TodoApp;
