import React, { Component } from 'react';
import { addTodo } from '../action/action';
import AddTodo from '../component/AddTodo';
import TodoList from '../component/TodoList';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';

class App extends Component {
  render () {
    return (
      <div>
        <h3>TODO</h3>
        <AddTodo addClick = {text => this.props.addTodo(text)}></AddTodo>
        <TodoList todoList={this.props.todoList}></TodoList>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    todoList: state.todos,
  };
}
const mapDispatchToProps = (dispatch) => {
 return {
   addTodo: bindActionCreators(addTodo, dispatch)
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
