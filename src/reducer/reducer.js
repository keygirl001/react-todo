import { ADD_TODO } from '../action/action.js';
import { combineReducers } from 'redux'

function todos(state = [], action) {
  switch (action.type) {
    case ADD_TODO: 
      return state.concat( [action.text] )
    default:
      return state
  }
}

const todoApp = combineReducers({
  todos
})
export default todoApp;
