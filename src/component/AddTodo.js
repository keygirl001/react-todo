import React, { Component } from 'react';

export default class AddTodo extends Component {
  render () {
    return (
      <div>
        <input ref='input' type='text'></input>
        <button onClick={(e) => this.handleClick(e)}>ADD</button>
      </div>
    )
  }

  handleClick(e) {
    const node = this.refs.input;
    const valueTodo = node.value;
    this.props.addClick(valueTodo);
    node.value = '';
  }
}
