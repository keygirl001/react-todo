import React, { Component } from 'react';

export default class TodoList extends Component {
  render() {
    const todoList = this.props.todoList;
    const itemList = todoList.map((item,index) => (
      <li key={index}>{item}</li>
    ))
    return (
      <ul>
        { itemList }
      </ul>
    )
  }
}
